// Number 2

db.fruits.aggregate([
    { $match: { onSale: true }},
    {
      $count: "total_fruits"
    }
  ]
);


// Number 3

db.fruits.aggregate([
    { $match: { stock: {$gt: 20} }},
    {
      $count: "stock"
    }
  ]
);


// Number 4

db.fruits.aggregate([
    { $match: { supplier_id: 1} },
     { $group: {
           _id: "Supplier1",
           avgPrice: { $avg: { $multiply: [ "$price" ] } } }
     }
]);

db.fruits.aggregate([
    { $match: { supplier_id: 2} },
     { $group: {
           _id: "Supplier2",
           avgPrice: { $avg: { $multiply: [ "$price" ] } } }
     }
]);


// Number 5

db.fruits.aggregate([
    { $match: { supplier_id: 1} },
     { $group: {
           _id: "Supplier1",
           maxPrice: { $max: { $multiply: [ "$price" ] } } }
     }
]);

db.fruits.aggregate([
    { $match: { supplier_id: 2} },
     { $group: {
           _id: "Supplier2",
           maxPrice: { $max: { $multiply: [ "$price" ] } } }
     }
]);


// Number 6

db.fruits.aggregate([
    { $match: { supplier_id: 1} },
     { $group: {
           _id: "Supplier1",
           minPrice: { $min: { $multiply: [ "$price" ] } } }
     }
]);

db.fruits.aggregate([
    { $match: { supplier_id: 2} },
     { $group: {
           _id: "Supplier2",
           minPrice: { $min: { $multiply: [ "$price" ] } } }
     }
]);